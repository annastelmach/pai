<?php

class User
{
    private $id;
    private $id_role;
    private $id_address;
    private $name;
    private $surname;
    private $email;
    private $password;
    private $role = "ROLE_USER";

    public function __construct($id,$id_role,$id_address,$surname, $name, $email, $password)
    {
        $this->id = $id;
        $this->id_role = $id_role;
        $this->id_address = $id_address;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->password = $password;
    }



    public function getIdAddress()
    {
        return $this->id_address;
    }


    public function setIdAddress($id_address)
    {
        $this->id_address = $id_address;
    }


    public function getIdRole()
    {
        return $this->id_role;
    }

    public function setIdRole($id_role)
    {
        $this->id_role = $id_role;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function setRole(string $role): void
    {
        $this->role = $role;
    }
}

class Address{
    private $kod_pocztowy;
    private $ulica;
    private $miasto;
    private $numer;


    public function __construct($kod_pocztowy, $ulica, $miasto, $numer)
    {
        $this->kod_pocztowy = $kod_pocztowy;
        $this->ulica = $ulica;
        $this->miasto = $miasto;
        $this->numer = $numer;
    }


    public function getKodPocztowy()
    {
        return $this->kod_pocztowy;
    }


    public function setKodPocztowy($kod_pocztowy): void
    {
        $this->kod_pocztowy = $kod_pocztowy;
    }


    public function getUlica()
    {
        return $this->ulica;
    }


    public function setUlica($ulica): void
    {
        $this->ulica = $ulica;
    }

    public function getMiasto()
    {
        return $this->miasto;
    }


    public function setMiasto($miasto): void
    {
        $this->miasto = $miasto;
    }


    public function getNumer()
    {
        return $this->numer;
    }


    public function setNumer($numer): void
    {
        $this->numer = $numer;
    }



}