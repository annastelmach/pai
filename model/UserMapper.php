<?php

require_once 'User.php';
require_once __DIR__.'/../Database.php';

class UserMapper
{
    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    public function getUser(string $email):User {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users where email = :email;');
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            return new User($user['id'],$user['id_role'],$user['id_address'],$user['name'], $user['surname'], $user['email'], $user['password']);
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    public function getUsers()
    {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users where email != :email;');
            $stmt->bindParam(':email', $_SESSION['id'], PDO::PARAM_STR);
            $stmt->execute();

            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $user;
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function delete(int $id): void
    {
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM users where id = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function addUser($name, $surname, $email, $password): void {
        try {
            $stmt = $this->database->connect()->prepare('INSERT INTO users(id,id_role,name,surname,email,password)
            VALUES ("NULL",1,:name,:surname,:email,:password)');
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->execute();

        }
        catch(PDOException $e) {
            die('nie udalo sie');
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function addAddress($kod_pocztowy, $miasto, $ulica, $numer,$id): void {
        try {
            $stmt = $this->database->connect()->prepare('INSERT INTO address(id_address,kod_pocztowy,miasto,ulica,numer,id)
            VALUES ("NULL",:kod_pocztowy,:miasto,:ulica,:numer,:id)');

            $stmt->bindParam(':kod_pocztowy', $kod_pocztowy, PDO::PARAM_STR);
            $stmt->bindParam(':miasto', $miasto, PDO::PARAM_STR);
            $stmt->bindParam(':ulica', $ulica, PDO::PARAM_STR);
            $stmt->bindParam(':numer', $numer, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->execute();

        }
        catch(PDOException $e) {
            die('nie udalo sie');
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getAllProduct(string $type):array {
        try {
            $stmt = $this->database->connect()->prepare('select produkty.* from produkty  inner join typ on produkty.id_typ=typ.id_typ AND typ.name=:type;');
            $stmt->bindParam(':type', $type, PDO::PARAM_STR);
            $stmt->execute();
            $exp = $stmt->fetchAll();
            if($exp == false){
                $arr = [];
                return $arr;
            }
            return $exp;
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }


    public function checkProduct($idprodukty):array
    {
        try {
            $stmt = $this->database->connect()->prepare('select p.* from produkty p where id_produkty= :id_produkty;');
            $stmt->bindParam(':id_produkty', $idprodukty, PDO::PARAM_STR);
            $stmt->execute();
            $exp = $stmt->fetch(PDO::FETCH_ASSOC);
            if($exp == false){
                $arr = [];
                return $arr;
            }
            return $exp;
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function showProduct($id):array
    {
        try {
            $stmt = $this->database->connect()->prepare('select p.* from produkty p inner join koszyk k on k.id_produkty=p.id_produkty AND k.id=:id ;');
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            $exp = $stmt->fetchAll();
            if($exp == false){
                $arr = [];
                return $arr;
            }
            return $exp;
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }



    public function deleteProduct($id_produkty):void {
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM koszyk where koszyk.id_produkty = :id_produkty;');
            $stmt->bindParam(':id_produkty', $id_produkty, PDO::PARAM_STR);
            $stmt->execute();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function addProduct($idprodukty, $id):void
    {
        try {
            $stmt = $this->database->connect()->prepare('INSERT INTO koszyk VALUES (null,:id_produkty,:id,1) ');
            $stmt->bindParam(':id_produkty', $idprodukty, PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function clearBasket($id):void {
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM koszyk where koszyk.id = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}