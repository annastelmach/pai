<?php
require_once __DIR__.'/../Database.php';
require_once(__DIR__.'/UserMapper.php');

class AdminMapper
{
    private $database;
    public function __construct()
    {
        $this->database = new Database();
    }
    public function getAllUsers($id):array {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users where id not like :id');
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetchAll();
            return $user;
        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    public function delete(int $id): void
    {
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM users WHERE id = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function deleteUser($id_user):void {
        $adminMapper = new AdminMapper();
        $allusers = $adminMapper->getAllUsers($id_user);
        foreach ($allusers as $id){
            $id = $id['id'];
            $adminMapper->delete($id);
        }
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM users WHERE id = :id_user;');
            $stmt->bindParam(':id_user', $id_user, PDO::PARAM_STR);
            $stmt->execute();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}