<?php

require_once "AppController.php";
require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';


class UserController extends AppController
{
    private $mapper = null;

    public function __construct()
    {
        parent::__construct();
        $this->mapper = new UserMapper();
    }

    public function addtobasket()
    {
        $id = $_GET["id_prod"];
        $ilosc = $this->mapper->checkProduct($id)['ilosc'];

        if ($ilosc > 1) {
            $this->mapper->addProduct($id, $_SESSION['id']);
           //$ilosc--;
        }

        $url = "http://localhost/pai/";
        header("Location: {$url}?page=koszyk");
        exit();
    }

    public function koszyk()
    {
        if(!isset($_SESSION['zalogowany'])) {
            $url = "http://localhost/pai/";
            header("Location: {$url}?page=index");
            exit();
        }
        $prodall = $this->mapper->showProduct($_SESSION["id"]);
        $_SESSION['list'] = $prodall;

        $this->render('koszyk');
    }

    public function potwierdzenie()
    {
        $mapper = new UserMapper();

        if ($this->isPost())
        {
            $allOK=true;

            $kod_pocztowy = $_POST['kod_pocztowy'];
            if (!is_numeric($kod_pocztowy))
            {
                $_SESSION['e_kod_pocztowy'] = "Kod pocztowy powinien być liczbą";
                $allOK=false;
            }

            $miasto = $_POST['miasto'];
            if (strlen($miasto)<3 || strlen($miasto)>20)
            {
                $_SESSION['e_name'] = "Miasto powinno posiadac od 3 do 20 liter";
                $allOK=false;
            }


            $ulica = $_POST['ulica'];
            if (strlen($ulica)<3 || strlen($ulica)>20)
            {
                $_SESSION['e_ulica'] = "Ulica powinna posiadać od 3 do 20 liter";
                $allOK=false;

            }

            $numer = $_POST['numer'];
            if (!is_numeric($numer))
            {
                $_SESSION['e_numer'] = "Numer powinien być liczbą";
                $allOK=false;

            }

            if ($allOK==true)
            {
                $mapper->addAddress($_POST['kod_pocztowy'], $_POST['miasto'], $_POST['ulica'], $_POST['numer'],$_SESSION['id']);

                $mapper->clearBasket($_SESSION['id']);

                $url = "http://localhost/pai/";
                header("Location: {$url}?page=powrot");
                exit();
            }
        }
        $this->render('potwierdzenie');
    }

    public function powrot(){
        $this->render('powrot');
    }

}