<?php

require_once "AppController.php";
require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';



class DefaultController extends AppController
{

    private $mapper = null;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new UserMapper();
    }

    public function index()
    {
        $text = 'Hello there 👋';

        $this->render('index', ['text' => $text]);
    }

    public function login()
    {
        $mapper = new UserMapper();

        $user = null;

        if ($this->isPost()) {

            $user = $mapper->getUser($_POST['email']);
            if(!$user) {

                return $this->render('login', ['message' => ['Email not recognized']]);
            }
            print_r($user->getPassword());
            if ($user->getPassword() !== md5($_POST['password'])) {

                return $this->render('login', ['message' => ['Wrong password']]);
            } else {
                $_SESSION["id"] = $user->getId();
                $_SESSION["id_role"] = $user->getIdRole();
                $_SESSION["email"] = $user->getEmail();
                $_SESSION["name"] = $user->getName()." ".$user->getSurname();
                $_SESSION["zalogowany"]=true;


                $url = "http://localhost/pai/";
                header("Location: {$url}?page=index");
                exit();
            }
        }

        $this->render('login');
    }


    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('index', ['text' => 'You have been successfully logged out!']);
    }

    public function register()
    {
        $mapper = new UserMapper();


        if ($this->isPost())
        {
                $allOK=true;

                $email = $_POST['email'];
                $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);
                if ((filter_var($emailB,FILTER_VALIDATE_EMAIL)==false) || ($emailB!=$email))
                {
                    $_SESSION['e_email'] = "Podaj poprawny adres e-mail";
                    $allOK=false;

                }

                $password=$_POST['password'];
                $psw_repeat=$_POST['psw_repeat'];

                if ($password !=$psw_repeat || strlen($password)<8 || strlen($password)>20)
                {
                    $_SESSION['e_psw']="Hasło powinno zawierać min 8 znaków, max 20 i hasła powinny być identyczne";
                    $allOK=false;
                }


                if (!isset($_POST['rules']))
                {
                    $_SESSION['e_rules']= "Proszę zaakceptować regulamin";
                    $allOK=false;
                }

                $name = $_POST['name'];
                if (strlen($name)<3 || strlen($name)>20)
                {
                    $_SESSION['e_name'] = "Imię powinno posiadac od 3 do 20 liter";
                    $allOK=false;
                }

                $surname = $_POST['surname'];
                if (strlen($surname)<3 || strlen($surname)>20)
                {
                    $_SESSION['e_surname'] = "Nazwisko powinno posiadać od 3 do 20 liter";
                    $allOK=false;

                }


                if($mapper->getUser($_POST['email'])->getEmail() == $_POST['email']){
                    $allOK = false;
                    $_SESSION['e_email'] = "Istnieje już konto przypisane do tego adresu email!";
                }


                if ($allOK==true)
                {
                    $mapper->addUser($_POST['name'], $_POST['surname'], $_POST['email'], md5($_POST['password']));
                    $url = "http://localhost/pai/";
                    header("Location: {$url}?page=login");
                    exit();
                }
        }
        $this->render('register');
    }

    public function information(){
        $this->render('information');
    }

    public function cennikp(){
        $prodall = $this->mapper->getAllProduct("projektor");
        $_SESSION['listAllProd'] = $prodall;
        $this->render('cennikp');
    }

    public function cennikm(){
        $prodall = $this->mapper->getAllProduct("glosnik");
        $_SESSION['listAllProd'] = $prodall;
        $this->render('cennikm');
    }

    public function cennikl(){
        $prodall = $this->mapper->getAllProduct("ekran");
        $_SESSION['listAllProd'] = $prodall;
        $this->render('cennikl');
    }

}