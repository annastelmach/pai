<?php

require_once "AppController.php";
require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';


class DeleteController extends AppController
{
    private $mapper = null;

    public function __construct()
    {
        parent::__construct();
        $this->mapper = new UserMapper();
    }

    public function delete():void
    {
        $id = $_GET["id_prod"];
        $this->mapper->deleteProduct($id);

        $url = "http://localhost/pai/";
        header("Location: {$url}?page=koszyk");
        exit();

    }
}
