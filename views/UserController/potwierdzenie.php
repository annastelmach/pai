<!DOCTYPE html>
<html lang="pl">
<?php
if (!(isset($_SESSION['id'])))
{
    header("Location: http://localhost/pai/?page=loginindex");
}
?>

<?php include(dirname(__DIR__).'/head.html'); ?>


<head>
    <title>Rentpol</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE"=edge,chrome=1"/>
    <script type="text/javascript" src="http://localhost/pai/public/js/timer.js"></script>
    <link rel="stylesheet" href="http://localhost/pai/public/css/style.css" />
    <link rel="stylesheet" href="http://localhost/pai/public/css/fontello.css" type="text/css"/>

</head>


<body onload="odliczanie();">

<div class="wrapper">

    <header>

        <ul class="Rectangle">
            <h1 class="Logo">
                RentPol
            </h1>
            <ul class="Information">
                <i class="icon icon-phone"></i>
                Kontakt
            </ul>
            <ul class="Number">
                888-888-888
            </ul>
            <div id="zegar"></div>
        </ul>

        <ul class="Name">
            Wypożyczalnia Sprzętu Multimedialnego
        </ul>

        <nav>
            <ul class="Menu">
                <li class="option"><a href="http://localhost/pai/?page=index">Strona Główna</a></li>
                <li class="option"><a href="http://localhost/pai/?page=information">O nas</a></li>
                <?php
                if(isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 1){
                    echo "
                        <li class=\"option\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"option1\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }else if (isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 0)
                {
                    echo "
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=index\">Admin</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }
                else
                {
                    echo "      
                <li class=\"option\"><a href=\"http://localhost/pai/?page=register\">Rejestracja</a></li>
                <li class=\"option\"><a href=\"http://localhost/pai/?page=login\">Logowanie</a></li>";
                }
                ?>
                <li style="clear:both"></li>
            </ul>
        </nav>
    </header>

    <main>
        <article class="Images">

            <form action="?page=potwierdzenie" method="post">
                <h1>Aby dokonać zamówienia wprowadź konieczne dane</h1>
                Miasto <input type="text" name="miasto" /></br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_miasto']))
                    {
                        echo $_SESSION['e_miasto'];
                        unset($_SESSION['e_miasto']);

                    }
                    ?>
                </div>

                Kod-pocztowy <input type="text" name="kod_pocztowy" /></br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_kod_pocztowy']))
                    {
                        echo $_SESSION['e_kod_pocztowy'];
                        unset($_SESSION['e_kod_pocztowy']);

                    }
                    ?>
                </div>

                Ulica <input type="text" name="ulica" /></br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_ulica']))
                    {
                        echo $_SESSION['e_ulica'];
                        unset($_SESSION['e_ulica']);

                    }
                    ?>
                </div>
                Numer <input type="text" name="numer" /> </br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_numer']))
                    {
                        echo $_SESSION['e_numer'];
                        unset($_SESSION['e_numer']);

                    }
                    ?>
                </div>

                <input type="submit" value="POTWIERDZAM ZAMÓWIENIE">

            </form>
        </article>
    </main>

    <div class="footer">
        RentPol-Wypożyczalnia Sprzętu Multimedialnego! &copy; Wszelkie prawa zasteżone
    </div>
</div>

</body>
</html>
