<!DOCTYPE html>
<html lang="pl">

<?php include(dirname(__DIR__).'/head.html'); ?>


<head>
    <title>Rentpol</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE"=edge,chrome=1"/>
    <script type="text/javascript" src="http://localhost/pai/public/js/timer.js"></script>
    <link rel="stylesheet" href="http://localhost/pai/public/css/style.css" />
    <link rel="stylesheet" href="http://localhost/pai/public/css/fontello.css" type="text/css"/>

</head>


<body onload="odliczanie();">

<div class="wrapper">

    <header>

        <ul class="Rectangle">
            <h1 class="Logo">
                RentPol
            </h1>
            <ul class="Information">
                <i class="icon icon-phone"></i>
                Kontakt
            </ul>
            <ul class="Number">
                888-888-888
            </ul>
            <div id="zegar"></div>
        </ul>

        <ul class="Name">
            Wypożyczalnia Sprzętu Multimedialnego
        </ul>

        <nav>
            <ul class="Menu">
                <li class="option"><a href="http://localhost/pai/?page=index">Strona Główna</a></li>
                <li class="option"><a href="http://localhost/pai/?page=information">O nas</a></li>
                <?php
                if(isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 1){
                    echo "
                        <li class=\"option\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"option\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }else if (isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 0)
                {
                    echo "
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=index\">Admin</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }
                else
                {
                    echo "      
                <li class=\"option\"><a href=\"http://localhost/pai/?page=register\">Rejestracja</a></li>
                <li class=\"option\"><a href=\"http://localhost/pai/?page=login\">Logowanie</a></li>";
                }
                ?>

                <li style="clear:both"></li>
            </ul>
        </nav>
    </header>

    <main>
        <article class="Images1">

            <div class="text">

                    Wypożyczalnia sprzętu multimedialnego NeedIT powstała w marcu 2015 r.
                    Nasi pracownicy posiadają długoletnie doświadczenie w branży multimedialnej poparte organizacją ponad 3000 imprez.
                    Zdobyta praktyka pozwala nam zrozumieć potrzeby szybko zmieniającego się rynku i dostosować do Państwa potrzeb.
                    Dzięki współpracy z nami nie będą Państwo musieli płacić za zakup drogiego sprzętu, który zostanie wykorzystany jednorazowo.
                    Pomożemy w wyborze urządzeń i dostarczymy wszystkie niezbędne rozwiązania na potrzeby szkolenia, konferencji czy imprezy firmowej.

                <br><br>Wypożyczalnia projektorów wynajmuje sprzęt komputerowy:
                <p><div style="display: list-item">wypożyczanie projektorów multimedialnych</div></p>
                <p><div style="display: list-item">wypożyczanie laptopów</div></p>
                <p><div style="display: list-item">wypożyczanie monitorów 19" panoramicznych</div></p>
                Wynajem sprzętu komputerowego świadczymy wyłącznie podmiotom gospodarczym.<br>
                Zastrzegamy sobie prawo do odmowy realizacji zamówienia bez podania przyczyny.<br>
            </div>
        </article>
    </main>

    <div class="footer">
        RentPol-Wypożyczalnia Sprzętu Multimedialnego! &copy; Wszelkie prawa zasteżone
    </div>
</div>

</body>
</html>
