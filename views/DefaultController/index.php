<!DOCTYPE html>
<html lang="pl">

<?php include(dirname(__DIR__).'/head.html'); ?>


<head>
    <title>Rentpol</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE"=edge,chrome=1"/>
    <script type="text/javascript" src="http://localhost/pai/public/js/timer.js"></script>
    <link rel="stylesheet" href="http://localhost/pai/public/css/style.css" />
    <link rel="stylesheet" href="http://localhost/pai/public/css/fontello.css" type="text/css"/>

</head>


<body onload="odliczanie();">

<div class="wrapper">

    <header>

        <ul class="Rectangle">
            <h1 class="Logo">
                RentPol
            </h1>
            <ul class="Information">
                <i class="icon icon-phone"></i>
                Kontakt
            </ul>
            <ul class="Number">
                888-888-888
            </ul>
            <div id="zegar"></div>
        </ul>


        <ul class="Name">
            Wypożyczalnia Sprzętu Multimedialnego
        </ul>

        <nav>
            <ul class="Menu">
                <li class="option"><a href="http://localhost/pai/?page=index">Strona Główna</a></li>
                <li class="option"><a href="http://localhost/pai/?page=information">O nas</a></li>
                <?php
                if(isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 1){
                    echo "
                        <li class=\"option\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"option\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }else if (isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 0)
                {
                    echo "
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=adminmenu\">Admin</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }
                else
                {
                    echo "      
                <li class=\"option\"><a href=\"http://localhost/pai/?page=register\">Rejestracja</a></li>
                <li class=\"option\"><a href=\"http://localhost/pai/?page=login\">Logowanie</a></li>";
                }
                ?>
                <li style="clear:both"></li>
            </ul>
        </nav>
    </header>

    <main>
        <article class="Images">

            <section class="Rzutnik">
                <div class="obrazek">
                    <i class="icon icon-videocam-outline"></i>
                </div>
                <div style="clear:both"></div>
                <div class="Text">Wynajem projektorów</div>
                <div style="clear:both"></div>
                <div class="Price"><a href="http://localhost/pai/?page=cennikp">Zobacz oferte</a></div>
            </section>

            <section class="Laptop">
                <div class="obrazek">
                    <i class="icon icon-volume"></i>
                </div>
                <div style="clear:both"></div>
                <div class="Text">Wynajem nagłośnienia</div>
                <div style="clear:both"></div>
                <div class="Price"><a href="http://localhost/pai/?page=cennikm">Zobacz oferte</a></div>
            </section>

            <section class="Monitor">
                <div class="obrazek">
                    <i class="icon icon-desktop"></i>
                </div>
                <div style="clear:both"></div>
                <div class="Text">Wynajem ekranów projekcyjnych</div>
                <div style="clear:both"></div>
                <div class="Price"><a href="http://localhost/pai/?page=cennikl">Zobacz oferte</a></div>
            </section>
        </article>
    </main>

    <div class="footer">
        RentPol-Wypożyczalnia Sprzętu Multimedialnego! &copy; Wszelkie prawa zasteżone
    </div>
</div>

</body>
</html>
