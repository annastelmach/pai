<!DOCTYPE html>
<html lang="pl">
<?php
if (isset($_SESSION['id']))
{
    header("Location: http://localhost/pai/?page=loginindex");
}
?>

<?php include(dirname(__DIR__).'/head.html'); ?>


<head>
    <title>Rentpol</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE"=edge,chrome=1"/>
    <script type="text/javascript" src="http://localhost/pai/public/js/timer.js"></script>
    <link rel="stylesheet" href="http://localhost/pai/public/css/style.css" />
    <link rel="stylesheet" href="http://localhost/pai/public/css/fontello.css" type="text/css"/>

</head>


<body onload="odliczanie();">

<div class="wrapper">

    <header>

        <ul class="Rectangle">
            <h1 class="Logo">
                RentPol
            </h1>
            <ul class="Information">
                <i class="icon icon-phone"></i>
                Kontakt
            </ul>
            <ul class="Number">
                888-888-888
            </ul>
            <div id="zegar"></div>
        </ul>

        <ul class="Name">
            Wypożyczalnia Sprzętu Multimedialnego
        </ul>

        <nav>
            <ul class="Menu">
                <li class="option"><a href="http://localhost/pai/?page=index">Strona Główna</a></li>
                <li class="option"><a href="http://localhost/pai/?page=information">O nas</a></li>
                <?php
                if(isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 1){
                    echo "
                        <li class=\"option\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"option1\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }else if (isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true && isset($_SESSION["id_role"]) && $_SESSION["id_role"] == 0)
                {
                    echo "
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=index\">Admin</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=koszyk\">Koszyk</a></li>
                        <li class=\"admin\"><a href=\"http://localhost/pai/?page=logout\">Wyloguj się</a></li>
                    ";
                }
                else
                {
                    echo "      
                <li class=\"option\"><a href=\"http://localhost/pai/?page=register\">Rejestracja</a></li>
                <li class=\"option\"><a href=\"http://localhost/pai/?page=login\">Logowanie</a></li>";
                }
                ?>
                <li style="clear:both"></li>
            </ul>
        </nav>
    </header>

    <main>
        <article class="Images">

            <form action="?page=register" method="post">
                <h1>Rejestracja</h1>
                Adres e-mail <input type="text" name="email" /></br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_email']))
                    {
                        echo $_SESSION['e_email'];
                        unset($_SESSION['e_email']);

                    }
                    ?>
                </div>

                Hasło <input type="password" name="password" /></br></br>
                Powtórz hasło <input type="password" name="psw_repeat" /></br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_psw']))
                    {
                        echo $_SESSION['e_psw'];
                        unset($_SESSION['e_psw']);

                    }
                    ?>
                </div>
                Imię <input type="text" name="name" /> </br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_name']))
                    {
                        echo $_SESSION['e_name'];
                        unset($_SESSION['e_name']);

                    }
                    ?>
                </div>

                Nazwisko <input type="text" name="surname" /></br></br>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_surname']))
                    {
                        echo $_SESSION['e_surname'];
                        unset($_SESSION['e_surname']);

                    }
                    ?>
                </div>
                
                <label >
                    <input type="checkbox" name="rules" /> *Zaakceptuj regulamin</br>
                </label>
                <div id="error">
                    <?php
                    if(isset($_SESSION['e_rules']))
                    {
                        echo $_SESSION['e_rules'];
                        unset($_SESSION['e_rules']);

                    }
                    ?>
                </div>

                <input type="submit" value="Zarejestruj się">

            </form>
        </article>
    </main>

    <div class="footer">
        RentPol-Wypożyczalnia Sprzętu Multimedialnego! &copy; Wszelkie prawa zasteżone
    </div>
</div>

</body>
</html>
