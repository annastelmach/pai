<?php

require_once 'controllers/DefaultController.php';
require_once 'controllers/PlayerController.php';
require_once 'controllers/AdminController.php';
require_once 'controllers/UserController.php';
require_once 'controllers/DeleteController.php';

class Routing
{
    public $routes = [];

    public function __construct()
    {
        $this->routes = [
            'index' => [
                'controller' => 'DefaultController',
                'action' => 'index'
            ],
            'login' => [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],
            'register' => [
                'controller' => 'DefaultController',
                'action' => 'register'
            ],
            'information' => [
                'controller' => 'DefaultController',
                'action' => 'information'

            ],
            'cennikp' => [
                'controller' => 'DefaultController',
                'action' => 'cennikp'
            ],
            'cennikm' => [
                'controller' => 'DefaultController',
                'action' => 'cennikm'
            ],
            'cennikl' => [
                'controller' => 'DefaultController',
                'action' => 'cennikl'
            ],
            'koszyk' => [
                'controller' => 'UserController',
                'action' => 'koszyk'
            ],
            'logout' => [
                'controller' => 'DefaultController',
                'action' => 'logout'
            ],

            'addtobasket' => [
                'controller' => 'UserController',
                'action' => 'addtobasket'
            ],

            'delete' => [
                'controller' => 'DeleteController',
                'action' => 'delete'

            ],
            'player' => [
                'controller' => 'PlayerController',
                'action' => 'player'
            ],
            'adminmenu' => [
                'controller' => 'AdminController',
                'action' => 'adminmenu'
            ],
            'admin_users' => [
                'controller' => 'AdminController',
                'action' => 'users'
            ],
            'admin_delete_user' => [
                'controller' => 'AdminController',
                'action' => 'userDelete'
            ]
            ,
            'deleteuser' => [
                'controller' => 'AdminController',
                'action' => 'deleteuser'
            ]
            ,
            'getAllUsers' => [
                'controller' => 'AdminController',
                'action' => 'getAllUsers'
            ]
            ,
            'potwierdzenie' => [
                'controller' => 'UserController',
                'action' => 'potwierdzenie'
            ]            ,
            'powrot' => [
                'controller' => 'UserController',
                'action' => 'powrot'
            ]

        ];
    }

    public function run()
    {
        $page = isset($_GET['page'])
            && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'login';

        if ($this->routes[$page]) {
            $class = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $class;
            $object->$action();
        }
    }

}